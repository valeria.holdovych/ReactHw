import React, {useEffect, useState} from 'react';
import FavoriteItem from "../FavoriteItem/FavoriteItem";
import styles from '../CardContainer/CardContainer.module.scss';

const FavoriteContainer = ({handleClick}) => {
    const [favorite, setFavorite] = useState([])

    const deleteFavoriteItem = (code) => {
        setFavorite((current) => {
            const favorite = [...current]

            const index = favorite.findIndex(el => el.code === code)


            if (index !== -1) {
                favorite.splice(index, 1);
            }

            localStorage.setItem('favorites', JSON.stringify(favorite))

            return favorite
        })
    }

    useEffect(() => {

        const favorite = localStorage.getItem('favorites')

        if (favorite) {
            setFavorite(JSON.parse(favorite))
        }
    }, [])

    return (
<div className='flex'>
            <ul className={styles.list}>
                {favorite.map(({name, price, img, count, id, code}) => {
                    return <FavoriteItem
                        deleteFavoriteItem={deleteFavoriteItem}
                        price={price}
                        key={code}
                        handleClick={handleClick}
                        name={name} img={img} count={count} id={id} code={code}/>
                })}
            </ul>
            </div>
    );
}

export default FavoriteContainer;

