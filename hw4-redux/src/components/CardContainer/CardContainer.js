import React from 'react';
import CardItem from "../CardItem";
import styles from './CardContainer.module.scss';
import PropTypes from "prop-types";
import { useSelector } from 'react-redux';


const CardContainer = ({addToCart, addToFavorites, handleClick}) => {

const cardsItems = useSelector(store => store.cards.cards)

		return (
			<div>
				<ul className={styles.list}>
					 {cardsItems.map(({name, img, code, price, id, isFavorite}) => (
						<li key={id}>
							<CardItem
							key={id}
							handleClick={handleClick}
								addToCart={addToCart}
								addToFavorites={addToFavorites}
								id={id}
								name={name}
								code={code}
                                price={price}
								img={img}
								isFavorite={isFavorite}
								/>
						</li>
					 ))}
				</ul>
			</div>
		);
	}



CardContainer.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    code: PropTypes.number,
    price: PropTypes.string,
    id: PropTypes.number,
    isFavorite: PropTypes.bool,
    changeFavorites: PropTypes.func,
}
export default CardContainer;

