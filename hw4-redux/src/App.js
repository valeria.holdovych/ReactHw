import React from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import AppRoutes from "./AppRoutes";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import store from "./store";


const App = () => {

    return (
        <Provider store={store}>
            <BrowserRouter>
                <div className={styles.App}>
                    <Header />
                    <AppRoutes className='wrapper' />
                </div>
            </BrowserRouter>
        </Provider>
    );
}



export default App;

