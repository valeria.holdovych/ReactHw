import { createSlice } from "@reduxjs/toolkit";

const cardsSlice = createSlice({
    name: 'cards',
    initialState: {
        cards: []
    },
    reducers: {
        addCards: (state, action) => {
            state.cards = action.payload;
        }
    }
})

export const {addCards} =  cardsSlice.actions;

const fetchCards = () => async (dispatch) => {
    const cards = await fetch('./shop.json').then(res => res.json());
 dispatch(addCards(cards))
}

export default cardsSlice.reducer;
export {fetchCards};