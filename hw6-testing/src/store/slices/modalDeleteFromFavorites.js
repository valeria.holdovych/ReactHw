import { createSlice } from "@reduxjs/toolkit";

const deleteFromFavoritesSlice = createSlice({
    name: 'ModalDeleteFromFavorites',
    initialState: {
        isOpen: false
    },
    reducers: {
        setIsOpen: (state, action) => {
            state.isOpen = action.payload;
        }
    }
})

export const {setIsOpen} =  deleteFromFavoritesSlice.actions;

export default deleteFromFavoritesSlice.reducer;