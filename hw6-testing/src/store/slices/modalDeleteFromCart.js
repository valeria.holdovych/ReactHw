import { createSlice } from "@reduxjs/toolkit";

const deleteFromCartSlice = createSlice({
    name: 'ModalDeleteFromCart',
    initialState: {
        isOpen: false
    },
    reducers: {
        setIsOpen: (state, action) => {
            state.isOpen = action.payload;
        }
    }
})

export const {setIsOpen} =  deleteFromCartSlice.actions;

export default deleteFromCartSlice.reducer;