import cardsReducer from  './slices/cardsSlice'
import {configureStore} from '@reduxjs/toolkit'
import  addToCartReducer  from './slices/modalAddToCart';
import  deleteFromCartReducer  from './slices/modalDeleteFromCart';
 import deleteFromFavoritesReducer from './slices/modalDeleteFromFavorites'


const store = configureStore({
    reducer: {
        cards: cardsReducer,
        addToCart: addToCartReducer,
        deleteFromCart: deleteFromCartReducer,
        deleteFromFavorites: deleteFromFavoritesReducer,
    }
}) 

export default store;