import React, {useEffect, useState} from 'react'
import CartContainer from '../components/CartContainer/CartContainer';
import CartForm from '../components/CartForm/CartForm';
import Modal from '../components/Modal';

const Carts = () => {

const [carts, setCarts] = useState([]);
const [isOpenModal, setIsOpenModal] = useState(false);
const [modalProps, setModalProps] = useState({})

const deleteCartItem = (id) => {
    setCarts((current) => {
        const carts = [...current]
        const index = carts.findIndex(el => el.id === id)
        if (index !== -1) {
            carts.splice(index, 1);
        }

        localStorage.setItem("carts", JSON.stringify(carts))
        return carts
    })

}

const incrementCartItem = (id) => {
    setCarts((current) => {
        const carts = [...current]

        const index = carts.findIndex(el => el.id === id)

        if (index !== -1) {
            carts[index].count += 1
        }

        localStorage.setItem("carts", JSON.stringify(carts))
        return carts
    })
}

const dicrementCartItem = (id) => {
    setCarts((current) => {
        const carts = [...current]

        const index = carts.findIndex(el => el.id === id)

        if (index !== -1 && carts[index].count > 1) {
            carts[index].count -= 1
        }

        localStorage.setItem("carts", JSON.stringify(carts))
        return carts
    })
}


const toggleModal = (value) => {
    setIsOpenModal(value)
}




useEffect(() => {

    const carts = localStorage.getItem('carts')

    if (carts) {
        setCarts(JSON.parse(carts))
    }
}, [])


    return (
        <div>
    <br></br>
    <br></br>
            <h2>Корзина:</h2>
            <CartContainer setModalProps={setModalProps}
             toggleModal={toggleModal} 
             incrementCartItem={incrementCartItem} 
             dicrementCartItem={dicrementCartItem} 
             carts={carts} />

    <div style={{ margin: "0 auto", width: "300px"}}>
    { carts.length >= 1 ? <CartForm carts={carts} /> : <p>Ваша корзина пуста</p>}
    </div>
             

            <Modal modalProps={modalProps} 
            deleteCartItem={deleteCartItem} 
            isOpen={isOpenModal} 
            toggleModal={toggleModal} />
            
        </div>
    )
}
export default Carts;