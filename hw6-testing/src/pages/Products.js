import React, {useEffect, useState, useContext} from 'react'
import styles from "../App.module.scss";
import CardContainer from '../components/CardContainer/CardContainer';
import AddModal from '../components/AddModal/AddModal';
import { useDispatch } from 'react-redux';
import { fetchCards } from '../store/slices/cardsSlice';
import Button from '../components/Button';
import { Context } from '../App';



const Products = () => {
    const [cards, setCards] = useState([])
    const [carts, setCarts] = useState([])
    const [isFavoriteButton, setIsFavoriteButton] = useState(false)
    const [favorites, setFavorites] = useState([])
    const [addModalCards, setAddModalCards] = useState({})
    const [modalActive, setModalActive] = useState(false)
    const {setCardsTheme} = useContext(Context);



    const addToCart = (card) => {
        setCarts((current) => {
            const carts = [...current]
            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({ ...card, count: 1 })
            } else {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return carts
        })
    }

    const handleClick = (name) => {
        setAddModalCards(() => {
            const newObj = {
                name,
            }
            return (newObj)
        })
        setModalActive(() => (!modalActive))
    }


    const addToFavorites = (favoriteCard) => {
        setFavorites((current) => {
            const favorites = [...current]

            const index = favorites.findIndex(el => el.code === favoriteCard.code)

            if (index === -1) {
                favorites.push({...favoriteCard, count: 1})
            } else {
                favorites.splice(index, 1)
            }

            localStorage.setItem('favorites', JSON.stringify(favorites))

            return favorites
        })
    }
 


const dispatch = useDispatch()

    useEffect(() => { 
            dispatch(fetchCards())
    }, [dispatch])
    
    return (
        <div>
           <section className={styles.leftContainer}>
                        <br />
                        <h1>Каталог наших поездок:</h1>
                        <Button onClick={()=> setCardsTheme(prev => !prev)}>поменять стилистику</Button>
                        <CardContainer 
                        handleClick={handleClick} addToFavorites={addToFavorites} addToCart={addToCart} cards={cards} />
                        {modalActive &&
                <AddModal
                    addToCart={addToCart}
                    clickHandler={handleClick}
                    addModalCards={addModalCards}
                />
            }

                    </section>
                   
         </div>
    )
}
export default Products;