import React, { createContext } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import AppRoutes from "./AppRoutes";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import store from "./store";
import { useState } from "react";

export const Context = createContext()

const App = () => {

const[ cardsTheme, setCardsTheme ] = useState(true);


    return (
        <Provider store={store}>
            <BrowserRouter>
            <Context.Provider value={{cardsTheme, setCardsTheme}}>
                <div className={styles.App}>
                    <Header />
                    <AppRoutes className='wrapper' />
                </div>
            </Context.Provider>
            </BrowserRouter>
        </Provider>
    );
}



export default App;

