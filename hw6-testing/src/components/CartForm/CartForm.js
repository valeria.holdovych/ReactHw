import React from "react";
import { Formik, Form } from 'formik'
import * as yup from 'yup'
import CustomInput from "./CustomInput/CustomInput";

//модалка?


function CartForm(carts, ) {


    const handleSubmit = (values, {resetForm}) => { 
        resetForm();
        localStorage.removeItem('carts');
        console.log(carts);
        console.log(values);
    }


    const initialValues = {
        name: '',
        surname:'',
        age:'',
        adress:'',
        phone: ''
    }

    const schema = yup.object().shape({
        name: yup.string()
            .min(2, "минимальное значение от 2х букв")
            .max(24, 'максимальное значение до 24 букв')
            .required(),
        surname: yup.string()
            .min(2, "минимальное значение от 2х букв")
            .max(24, 'максимальное значение до 24 букв')
            .required(),
        age: yup.number()
            .min(16, "вы можете совершить покупку с 16 лет")
            .max(90, "вы можете совершить покупку до 90 лет")
            .required(),
        adress: yup.string().required(),
        phone: yup.string().required(), 
    });


    return (
        <>
        <div className="form">
            <h1>Хотите оформить заказ? </h1>
            <h2>Заполните форму: </h2>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={schema}
            >
                {({
                    isValid,
                }) => (
                <Form>
                    <CustomInput name='name' type='text' placeholder='Имя' />
                    <CustomInput name='surname' type='text' placeholder='Фамилия' />
                    <CustomInput name='age' type='text' placeholder='Возраст' />
                    <CustomInput name='adress' type='text' placeholder='Адрес' />
                    <CustomInput name='phone' type='text' placeholder='Телефон' />
                    
                    <button onClick={()=>{}} type='submit' disabled={!isValid}>Оформить заказ</button>
                </Form>
                )}
            </Formik>
            </div>
        </>
    )
}

export default CartForm;