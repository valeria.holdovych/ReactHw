import styles from './Modal.module.scss';
import Button from '../Button'
import { useDispatch } from 'react-redux';
import { setIsOpen } from '../../store/slices/modalDeleteFromCart';


const Modal = ({isOpen, toggleModal, deleteCartItem, modalProps}) => {
        
const dispatch = useDispatch()

        if (!isOpen) {
            return null;
        }

        return (
            <div className={styles.root} data-testid="modal-root">
                <div  onClick={() => toggleModal(false)} className={styles.background}/>
                <div className={styles.content}>
                    <div className={styles.closeWrapper}>
                        <Button onClick={() => {
                            toggleModal(false) 
                            dispatch(setIsOpen(false))
                        }} 
                        className={styles.btn} color="red">X</Button>
                    </div>
                    <h2>Вы хотите удалить товар с корзины {modalProps.title}?</h2>
                    <div className={styles.buttonContainer}>
                        <Button onClick={() => {
                            deleteCartItem(modalProps.id);
                            toggleModal(false);
                            dispatch(setIsOpen(false))
                        }}>Да</Button>
                        <Button onClick={() => {
                            toggleModal(false)
                            dispatch(setIsOpen(false))
                            }} color="red">Нет</Button>
                    </div>
                </div>
            </div>
        );
    }

export default Modal;


