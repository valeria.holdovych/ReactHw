import Modal from "./Modal";
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider, useDispatch } from 'react-redux';
import store from "../../store";
import { setIsOpen } from "../../store/slices/modalDeleteFromCart";


const MockedProvider = ({children}) => {
    return (
        <Provider store={store} >
        <Component />
    </Provider>
    )

}

const Component = () => {
    const dispatch = useDispatch();
    
    return (
            <>
            <button onClick={
                () => {dispatch(setIsOpen(true))}}
                >OPEN</button>
            <Modal />
            </>

    )
}


describe('Modal render', () => {
    test('should Modal match snapshot', () => {
        const { asFragment } = render(<MockedProvider/>)
        expect(asFragment()).toMatchSnapshot()
    })
})

describe('Modal open on state chnages', () => {
    test('should Modal close on background click', () => {
        render(<MockedProvider />);

        fireEvent.click(screen.getByText('OPEN'));
    
        expect(screen.getByTestId('modal-root')).toBeInTheDocument();
    });
})