import Button from './Button';
import {  render,  } from '@testing-library/react';

// const handleClick = jest.fn()


describe('button snapshot testing', () => {
test('should button match snapshot', () => { 
   const {asFragment} =  render(<Button></Button>);
   expect(asFragment()).toMatchSnapshot();
 })
});


// describe('handle click on a button', () => {
//     test('should handleclick work', () => { 
//       render(<Button type={'submit'} handleClick={handleClick} className='btn'>Submit</Button>);
//     const btn = screen.getByText('button');
//     fireEvent.click(btn);
//     expect(handleClick).toHaveBeenCalled();
//      })
//     })