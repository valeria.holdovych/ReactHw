import {  setIsOpen } from '../../store/slices/modalAddToCart'
import modalAddToCart from '../../store/slices/modalAddToCart'
const initialState = {
    isOpen: false
}


describe('Modal reduser works correct', ()=>{
    test('should return the initial state', () => {
        expect(modalAddToCart(undefined, { type: undefined })).toEqual(initialState)
      })

      test('should change isOpen', () => {
        expect(modalAddToCart(initialState, { type:  setIsOpen, payload: false})).toEqual(initialState)
      })
      
})