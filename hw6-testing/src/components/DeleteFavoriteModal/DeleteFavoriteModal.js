import React from 'react'
import { useDispatch } from 'react-redux'
import { setIsOpen } from '../../store/slices/modalDeleteFromFavorites'
import styles from './DeleteFavoriteModal.module.scss'

const AddModal = ({backgroundColor, clickHandler, deleteFavoriteModal: {name}}) => {

const dispatch = useDispatch()

    return (
        <div className={styles.modal} onClick={clickHandler}>
            <div style={{backgroundColor}} className={styles.content} onClick={(e) => {
                e.stopPropagation()
            }}>
                <div className={styles.header}>

                </div>
                <p>товар {name} убран с выбранных</p>
                <div className={styles.buttons}>
                    <button onClick={() => {
                        clickHandler()
                        dispatch(setIsOpen(false))
                    }}
                    >OK
                    </button>
                </div>
            </div>
        </div>
    )
}

export default AddModal
