import React from "react";
import styles from "./Button.module.scss";

class Button extends React.PureComponent {
    render() {
        const {handleClick, text, backgroundColor} = this.props;
        const {button} = styles;
        return (
            <button
                className={button}
                style={{backgroundColor}}
                onClick={handleClick}>{text}
            </button>
        );
    }
}

export default Button;
