import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
    render() {
        const {
            modal_text,
            modal_buttons,
            modal_component,
            modal_close,
            modal_background,
        } = styles;
        const { backgroundColor, handleClick, header, text, closeBtn, actions } = this.props;
        return (
            <div className={modal_background} onClick={handleClick} >
                <div style={{backgroundColor}} className={modal_component} onClick={(e) => e.stopPropagation()}>
                    <div>
                        <h2>{header}</h2>
                        {closeBtn && (
                            <div className={modal_close} onClick={handleClick}></div>
                        )}
                    </div>
                    <p className={modal_text}>{text}</p>
                    <div className={modal_buttons}>{actions}</div>
                </div>
            </div>
        );
    }
}

export default Modal;

