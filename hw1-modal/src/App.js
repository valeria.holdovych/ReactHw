import styles from "./App.module.scss";
import React from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
class App extends React.PureComponent {
  state = {
    openModal: true,
    choosedBtn: "",
  };
  handleClick = (e) => {
    if (e.target.textContent === "Open first modal") {
      this.setState((prev) => ({
        openModal: !prev.openModal,
        choosedBtn: "first Modal",
      }));
    } else if (e.target.textContent === "Open second modal") {
      this.setState((prev) => ({
        openModal: !prev.openModal,
        choosedBtn: "second Modal",
      }));
    } else {
      this.setState((prev) => ({
        openModal: !prev.openModal,
      }));
    }
  };

  render() {
    const modalStatus = this.state.openModal;
    const choosedBtn = this.state.choosedBtn;
    return (
        <div className={styles.wrapper}>
          <div>
            <Button
                backgroundColor="blue"
                handleClick={this.handleClick}
                text="Open first modal"
            />
            <Button
                backgroundColor="yellow"
                handleClick={this.handleClick}
                text="Open second modal"
            />
          </div>
          {!modalStatus && choosedBtn === "first Modal" && (
              <Modal
                  handleClick={this.handleClick}
                  header="Do you really want to delete this file? "
                  text='Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
                  closeBtn={true}
                  actions={
                    <>
                      <button onClick={this.handleClick}>Ok</button>
                      <button onClick={this.handleClick}>Cancel</button>
                    </>
                  }
              />
          )}
          {!modalStatus && choosedBtn === "second Modal" && (
              <Modal
                  backgroundColor='grey'
                  handleClick={this.handleClick}
                  header="are you serious?"
                  text='if yes i will do this'
                  closeBtn={false}
                  actions={
                    <>
                      <button onClick={this.handleClick}>Confirm</button>
                      <button onClick={this.handleClick}>Go back</button>
                    </>
                  }
              />
          )}
        </div>
    );
  }
}

export default App;
