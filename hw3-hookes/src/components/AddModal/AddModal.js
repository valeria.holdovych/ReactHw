import React from 'react'
import styles from './AddModal.module.scss'

const AddModal = ({backgroundColor, clickHandler, addModalCards: {name}}) => {

    return (
        <div className={styles.modal} onClick={clickHandler}>
            <div style={{backgroundColor}} className={styles.content} onClick={(e) => {
                e.stopPropagation()
            }}>
                <div className={styles.header}>

                </div>
                <p>товар {name} добавлен в корзину</p>
                <div className={styles.buttons}>
                    <button onClick={() => {
                        clickHandler()
                    }}
                    >OK
                    </button>
                </div>
            </div>
        </div>
    )
}

export default AddModal
