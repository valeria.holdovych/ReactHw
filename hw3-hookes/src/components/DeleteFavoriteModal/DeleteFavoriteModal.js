import React from 'react'
import styles from './DeleteFavoriteModal.module.scss'

const AddModal = ({backgroundColor, clickHandler, deleteFavoriteModal: {name}}) => {

    return (
        <div className={styles.modal} onClick={clickHandler}>
            <div style={{backgroundColor}} className={styles.content} onClick={(e) => {
                e.stopPropagation()
            }}>
                <div className={styles.header}>

                </div>
                <p>товар {name} убран с выбранных</p>
                <div className={styles.buttons}>
                    <button onClick={() => {
                        clickHandler()
                    }}
                    >OK
                    </button>
                </div>
            </div>
        </div>
    )
}

export default AddModal
