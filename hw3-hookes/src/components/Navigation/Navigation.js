import { NavLink } from "react-router-dom";

const Navigation = () => {
    return(
        <nav>
        <ul>
        <li>
                <NavLink className={({ isActive }) => isActive && 'active-link'} to="/">На Главную</NavLink>
            </li>
            <li>
                <NavLink className={({ isActive }) => isActive && 'active-link'} to="/favorites">Избранное</NavLink>
            </li>
            <li>
                <NavLink className={({ isActive }) => isActive && 'active-link'} to="/carts">Корзина</NavLink>
            </li>
        </ul>
    </nav>
    )
}
export default Navigation