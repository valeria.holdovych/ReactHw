import React from 'react';
import CartItem from '../CartItem/'
import PropTypes from "prop-types";


const CartContainer  = ({carts, incrementCartItem, dicrementCartItem, toggleModal, setModalProps}) => {

        return (
            <ul>
                {carts.map(({name, img, count, id}) => {

                    return <CartItem setModalProps={setModalProps} toggleModal={toggleModal} name={name} img={img}
                                     count={count} id={id} incrementCartItem={incrementCartItem}
                                     dicrementCartItem={dicrementCartItem}/>
                })}
            </ul>
        )
    }


CartContainer.propTypes = {
    carts: PropTypes.array,
    incrementCartItem: PropTypes.func,
    dicrementCartItem: PropTypes.func,
    toggleModal: PropTypes.func,
    setModalProps: PropTypes.func,
    name: PropTypes.string,
    img: PropTypes.string,
    count: PropTypes.number,
    id: PropTypes.number,
}
export default CartContainer;

