import React from 'react';
import CardItem from "../CardItem/";
import styles from './CardContainer.module.scss';
import PropTypes from "prop-types";


const CardContainer = ({addToCart, cards, addToFavorites, handleClick}) => {

		return (
			<div>
				<ul className={styles.list}>
					 {cards.map(({name, img, code, price, id, isFavorite}) => (
						<li key={id}>
							<CardItem
							handleClick={handleClick}
								addToCart={addToCart}
								addToFavorites={addToFavorites}
								id={id}
								name={name}
								code={code}
                                price={price}
								img={img}
								isFavorite={isFavorite}
								/>
						</li>
					 ))}
				</ul>
			</div>
		);
	}



CardContainer.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    code: PropTypes.number,
    price: PropTypes.string,
    id: PropTypes.number,
    isFavorite: PropTypes.bool,
    changeFavorites: PropTypes.func,
}
export default CardContainer;

