import React from 'react';
import styles from "../CardItem/CardItem.module.scss";
import heartSvg from '../../svg/heart.svg';

const FavoriteItem = ({img, name, code, deleteFavoriteItem, handleClick }) => {

    return (
        <div className={styles.card}>
            <button onClick={() => {
                deleteFavoriteItem(code);
                handleClick({name})
            }} type="button" className={styles.likeButton}>
                <img src={heartSvg} alt='favorite'/>
            </button>
            <span className={styles.title}>{name}</span>
            <img className={styles.itemAvatar} src={img}
                 alt={name}/>
            <span className={styles.description}>code: {code}</span>
            
        </div>
    );

}

export default FavoriteItem;
