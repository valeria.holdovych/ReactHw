import React, {useEffect, useState} from 'react'
import styles from "../App.module.scss";
import CardContainer from '../components/CardContainer/CardContainer';
import AddModal from '../components/AddModal/AddModal';



const Products = () => {
    const [cards, setCards] = useState([])
    const [carts, setCarts] = useState([])
    const [favorites, setFavorites] = useState([])
    const [addModalCards, setAddModalCards] = useState({})
    const [modalActive, setModalActive] = useState(false)


    const addToCart = (card) => {
        setCarts((current) => {
            const carts = [...current]
            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({ ...card, count: 1 })
            } else {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return carts
        })
    }

    const handleClick = (name) => {
        setAddModalCards(() => {
            const newObj = {
                name,
            }
            return (newObj)
        })
        setModalActive(() => (!modalActive))
    }



    const addToFavorites = (favoriteCard) => {
        setFavorites((current) => {
            const favorites = [...current]

            const index = favorites.findIndex(el => el.code === favoriteCard.code)

            if (index === -1) {
                favorites.push({...favoriteCard, count: 1})
            } else {
                favorites.splice(index, 1)
            }

            localStorage.setItem('favorites', JSON.stringify(favorites))

            return favorites
        })
    }


    useEffect(() => {
        const getData = async () => {
            const cards = await fetch('./shop.json').then(res => res.json());
            const carts = localStorage.getItem('carts')
        if (carts) {
            setCarts(JSON.parse(carts))
        }
    
        setCards(cards);
    }
        getData()
    }, [])
    
    return (
        <div>
           <section className={styles.leftContainer}>
                        <br />
                        <h1>Каталог наших поездок:</h1>
                        <CardContainer handleClick={handleClick} addToFavorites={addToFavorites} addToCart={addToCart} cards={cards} />
                        {modalActive &&
                <AddModal
                    addToCart={addToCart}
                    clickHandler={handleClick}
                    addModalCards={addModalCards}
                />
            }

                    </section>
                   
         </div>
    )
}
export default Products;