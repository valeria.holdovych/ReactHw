import React, {useState} from 'react'
import styles from "../App.module.scss";
import FavoriteContainer from '../components/FavoriteContainer/FavoriteContainer'
import DeleteFavoriteModal from '../components/DeleteFavoriteModal/DeleteFavoriteModal'

const Favorites = () => {

const [deleteFavoriteModal, setDeleteFavoritesModal] = useState(false);

const handleClick = (name) => {
    setDeleteFavoritesModal(() => {
        const newObj = {
            name,
        }
        return (newObj)
    })
    setDeleteFavoritesModal(() => (!deleteFavoriteModal))
}

    return (
        <section>
            <br></br>
            <br></br>
            <h2>Вам понравилось:</h2>
            <FavoriteContainer handleClick={handleClick}/>
{ deleteFavoriteModal && <DeleteFavoriteModal clickHandler={handleClick} deleteFavoriteModal={deleteFavoriteModal}/>}
        </section>
    );
};

export default Favorites;

