import React, { useState } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import AppRoutes from "./AppRoutes";


const App = () => {
   

    return (
        <div className={styles.App}>

            <Header  />
            <AppRoutes className='wrapper' />


        </div>
    );
}



export default App;

