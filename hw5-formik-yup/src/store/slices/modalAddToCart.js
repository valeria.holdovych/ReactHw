import { createSlice } from "@reduxjs/toolkit";

const addToCartSlice = createSlice({
    name: 'ModalAddToCart',
    initialState: {
        isOpen: false
    },
    reducers: {
        setIsOpen: (state, action) => {
            state.isOpen = action.payload;
        }
    }
})

export const {setIsOpen} =  addToCartSlice.actions;

export default addToCartSlice.reducer;