import React, {useState} from 'react'
import FavoriteContainer from '../components/FavoriteContainer/FavoriteContainer'
import DeleteFavoriteModal from '../components/DeleteFavoriteModal/DeleteFavoriteModal'

const Favorites = () => {

    const [carts, setCarts] = useState([])

    const addToCart = (card) => {
        setCarts((current) => {
            const carts = [...current]
            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({ ...card, count: 1 })
            } else {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return carts
        })
    }

const [deleteFavoriteModal, setDeleteFavoritesModal] = useState(false);

const handleClick = (name) => {
    setDeleteFavoritesModal(() => {
        const newObj = {
            name,
        }
        return (newObj)
    })
    setDeleteFavoritesModal(() => (!deleteFavoriteModal))
}

    return (
        <section>
            <br></br>
            <br></br>
            <h2>Вам понравилось:</h2>
            <FavoriteContainer handleClick={handleClick} addToCart={addToCart}/>
{ deleteFavoriteModal && <DeleteFavoriteModal clickHandler={handleClick} deleteFavoriteModal={deleteFavoriteModal}/>}
        </section>
        
    );
};

export default Favorites;

