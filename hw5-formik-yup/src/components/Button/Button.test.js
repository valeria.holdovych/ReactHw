import Button from './Button';
import { render } from '@testing-library/react';

describe('button snapshot testing', () => {
test('should button match snapshot', () => { 
   const {asFragment} =  render(<Button/>);
   expect(asFragment()).toMatchSnapshot();
 })
})