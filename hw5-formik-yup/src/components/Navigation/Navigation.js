import { NavLink } from "react-router-dom";

const Navigation = () => {
    return(
        <nav>
        <ul>
        <li>
                <NavLink  to="/">На Главную</NavLink>
            </li>
            <li>
                <NavLink  to="/favorites">Избранное</NavLink>
            </li>
            <li>
                <NavLink  to="/carts">Корзина</NavLink>
            </li>
        </ul>
    </nav>
    )
}
export default Navigation