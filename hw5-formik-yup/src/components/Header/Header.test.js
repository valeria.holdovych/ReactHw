import Header from "./Header";
import { render } from '@testing-library/react';

describe('header snapshot testing', () => {
test('should header match snapshot', () => { 
   const {asFragment} =  render(<Header/>);
   expect(asFragment()).toMatchSnapshot();
 })
})