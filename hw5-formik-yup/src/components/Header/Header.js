import styles from './Header.module.scss'
import React from 'react';
import cartIcon from '../../svg/cart-outline.svg'
import PropTypes from "prop-types";

import Navigation from '../Navigation/Navigation';

const Header = () => {

        return (
            <header  className={styles.root}>
                <div>
                    <br/>
    <h1>TRIPSHOP</h1>
                </div>


               <Navigation/>

                <ul>
                    <li>
                        <a href="https://google.com"><img src={cartIcon} alt="Cart" /></a>
                    </li>
                </ul>
            </header>
        );
    }


Header.propTypes = {
    cartCounter: PropTypes.number,
  
}

export default Header;
