import React from 'react';
import styles from "../CardItem/CardItem.module.scss";
import heartSvg from '../../svg/heart.svg';
import { useDispatch } from 'react-redux';
import { setIsOpen } from '../../store/slices/modalDeleteFromFavorites';
import Button from '../Button';


const FavoriteItem = ({img, name, code, deleteFavoriteItem, handleClick, id, addToCart}) => {
    const dispatch = useDispatch()
    return (
        <div className={styles.card}>
            <button onClick={() => {
                dispatch(setIsOpen(true))
                deleteFavoriteItem(code);
                handleClick({name})
            }} type="button" className={styles.likeButton}>
                <img src={heartSvg} alt='favorite'/>
            </button>
            <span className={styles.title}>{name}</span>
            <img className={styles.itemAvatar} src={img}
                 alt={name}/>
            <span className={styles.description}>code: {code}</span>
            <Button onClick={()=>{
                        dispatch(setIsOpen(true))
                        addToCart({name, img, id})
                    }}>Выбрать тур</Button>
            
        </div>
    );

}

export default FavoriteItem;
