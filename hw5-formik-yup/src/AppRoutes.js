import {Routes, Route} from 'react-router-dom';
import Carts from './pages/Carts';
import Favorites from './pages/Favorites';
import Products from './pages/Products';

const AppRoutes = () => {

    return(
<Routes>
    <Route path='/' element={<Products/>}></Route>
    <Route path='/favorites' element={<Favorites/>}></Route>
    <Route path='/carts' element={<Carts />}></Route>
</Routes>
    )
}

export default AppRoutes;