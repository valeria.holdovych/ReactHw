import React,{ Component } from "react";
import styles from './App.module.scss';
import Header from './components/Header';
import Modal from './components/Modal';
import CardContainer from "./components/CardContainer/CardContainer";
import CartContainer from "./components/CartContainer/CartContainer";

class App extends Component {
    state = {
        isFavorite: false,
        cards: [],
        carts: [],
        isOpenModal: false,
        modalProps: {},
        modalActive: false,
        favorites: [],
    }


    addToCart = (card) => {
        this.setState((current) => {
            const carts = [...current.carts]
            const index = carts.findIndex(el => el.id === card.id)

            if (index === -1) {
                carts.push({ ...card, count: 1 })
            } else {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })
    }

    changeFavorites = (value) => {
        this.setState({ isFavorite: value })
    }

    addToFavorites = (favoriteCard) => {
        this.setState((current) => {
            const favorites = [...current.favorites]

            const index = favorites.findIndex(el => el.code === favoriteCard.code)

            if (index === -1) {
                favorites.push({...favoriteCard, count: 1})
            } else {
                favorites.splice(index, 1)
            }

            localStorage.setItem('favorites', JSON.stringify(favorites))

            return {favorites}
        })
    }


    incrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1) {
                carts[index].count += 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })
    }

    dicrementCartItem = (id) => {
        this.setState((current) => {
            const carts = [...current.carts]

            const index = carts.findIndex(el => el.id === id)

            if (index !== -1 && carts[index].count > 1) {
                carts[index].count -= 1
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })
    }

    deleteCartItem = (id) => {
        this.setState((current) => {
        const carts = [...current.carts]
            const index = carts.findIndex(el => el.id === id)
            if (index !== -1) {
                carts.splice(index, 1);
            }

            localStorage.setItem("carts", JSON.stringify(carts))
            return { carts }
        })

    }

    toggleModal = (value) => {
        this.setState({ isOpenModal: value })
    }

    setModalProps = (value) => {
        this.setState({ modalProps: value })
    }


    async componentDidMount() {
        const cards = await fetch('./shop.json').then(res => res.json());
        const carts = localStorage.getItem('carts')

        if(carts){
            this.setState({carts: JSON.parse(carts)})
        }

        this.setState({ cards: cards });

    }

    render() {
        const { isFavorite, cards, carts, isOpenModal, modalProps } = this.state;
        return (
            <div className={styles.App}>
                <Header carts={carts}/>
                <main>
                    <section className={styles.leftContainer}>
                        <br/>
                        <h1>Каталог наших поездок:</h1>
                        <CardContainer isFavorite={isFavorite} changeFavorites={this.changeFavorites} addToFavorites={this.addToFavorites} addToCart={this.addToCart} cards={cards}/>
                    </section>
                    <section className={styles.rightContainer}>
                        <br/>
                        <h2>Корзина:</h2>
                        <CartContainer setModalProps={this.setModalProps} toggleModal={this.toggleModal} incrementCartItem={this.incrementCartItem} dicrementCartItem={this.dicrementCartItem} carts={carts} />
                        </section>
                </main>

                <Modal modalProps={modalProps} deleteCartItem={this.deleteCartItem} isOpen={isOpenModal} toggleModal={this.toggleModal} />
                 </div>
        );
    }

}

export default App;
