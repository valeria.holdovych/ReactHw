import React, {PureComponent} from 'react';
import CardItem from "../CardItem/";
import styles from './CardContainer.module.scss';
import PropTypes from "prop-types";


class CardContainer extends PureComponent {
	render() {
		const {addToCart, cards} = this.props;
		console.log(cards);
		return (
			<div>
				<ul className={styles.list}>
					 {cards.map(({name, img, code, price, id, isFavorite, changeFavorites}) => (
						<li key={id}>
							<CardItem
							addToCart={addToCart}
								id={id}
								name={name}
								code={code}
                                price={price}
                                changeFavorites={changeFavorites}
								img={img}
								isFavorite={isFavorite}
								/>
						</li>
					 ))}
				</ul>
			</div>
		);
	}
}


CardContainer.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    code: PropTypes.number,
    price: PropTypes.string,
    id: PropTypes.number,
    isFavorite: PropTypes.bool,
    changeFavorites: PropTypes.func,
}
export default CardContainer;

