import {PureComponent} from 'react';
import styles from './Header.module.scss'

import cartIcon from '../../svg/cart-outline.svg'
import PropTypes from "prop-types";

class Header extends PureComponent {

    render() {
        const { carts } = this.props;
        const cartCounter = carts.length;
        console.log(carts)
        return (
            <header  className={styles.root}>
                <div>
                    <br/>
    <h1>TRIPSHOP</h1>
                </div>

                <ul>
                    <li>
                        <a href="#"><img src={cartIcon} alt="Cart" /></a>
                    </li>
                    <li>{cartCounter}</li>
                </ul>
            </header>
        );
    }
}

Header.propTypes = {
    cartCounter: PropTypes.number,
    cartIcon: PropTypes.element,
}

export default Header;
