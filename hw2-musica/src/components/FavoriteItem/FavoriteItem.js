import {PureComponent} from 'react';

class FavoriteItem extends PureComponent {
    render() {

        const {img, name} = this.props

        return (
            <div>
                <img src={img} alt={name}/>
            </div>
        );
    }
}

export default FavoriteItem;
