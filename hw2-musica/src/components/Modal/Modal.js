import {PureComponent} from 'react';
import styles from './Modal.module.scss';
import Button from '../Button'

class Modal extends PureComponent {

    render() {
        const { isOpen, toggleModal, deleteCartItem, modalProps } = this.props;

        if (!isOpen) {
            return null;
        }

        return (
            <div className={styles.root}>
                <div onClick={() => toggleModal(false)} className={styles.background}/>
                <div className={styles.content}>
                    <div className={styles.closeWrapper}>
                        <Button onClick={() => toggleModal(false)} className={styles.btn} color="red">X</Button>
                    </div>
                    <h2>Do you really want to delete {modalProps.title}?</h2>
                    <div className={styles.buttonContainer}>
                        <Button onClick={() => {
                            deleteCartItem(modalProps.id);
                            toggleModal(false);
                        }}>Yes</Button>
                        <Button onClick={() => toggleModal(false)} color="red">No</Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;


