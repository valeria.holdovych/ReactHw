import { Component } from 'react';
import styles from './CardItem.module.scss';
import heartSvg from '../../svg/heart.svg';
import heartSvgOutline from '../../svg/heart-outline.svg';
import Button from '../Button';
import PropTypes from "prop-types";



class CardItem extends Component {

    state = {
        btnImage:  true,
    }

    render() {
        const {btnImage} = this.state;
        const {addToFavorites, addToCart, name, img, price, code, id} = this.props;
        return (

            <div className={styles.card}>
                <button onClick={() => {
                    this.setState((prev) => ({
                        btnImage: !prev.btnImage
                    }))
                    addToFavorites({name, img, code}
                    )}} type="button" className={styles.likeButton}>
                <img  src={btnImage ? heartSvgOutline: heartSvg} alt="favorite"/>
                </button>
                <span className={styles.title}>{name}</span>
                <img className={styles.itemAvatar} src={img}
                     alt=''/>
                <span className={styles.description}>{price}</span>
                <span className={styles.description}>{code}</span>
                <div className={styles.btnContainer}>
                    <Button onClick={()=>{
                        addToCart({name, img, id})
                    }}>Выбрать тур</Button>
                </div>
            </div>
        )
    }
}

CardItem.propTypes = {
    name: PropTypes.string,
    img: PropTypes.string,
    code: PropTypes.number,
    price: PropTypes.string,
    id: PropTypes.number,
    isFavorite: PropTypes.bool,
    changeFavorites: PropTypes.func,
    addToCart: PropTypes.func,
}


export default CardItem;

