import {PureComponent} from 'react';
import FavoriteItem from "../FavoriteItem/FavoriteItem";

class FavoriteContainer extends PureComponent {

    render() {

        const {favorites} = this.props

        return (
            <ul>
                {favorites.map(({img, code, name}) => {
                    return (
                        <FavoriteItem key={code} img={img} code={code} name={name}/>)
                })}
            </ul>
        );
    }
}

export default FavoriteContainer;
