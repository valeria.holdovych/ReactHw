import {PureComponent} from 'react';
import styles from './CartItem.module.scss';
import Button from '../Button'
import PropTypes from "prop-types";

class CartItem extends PureComponent {

    render() {

        const { name, img, count, id, incrementCartItem, dicrementCartItem, toggleModal, setModalProps } = this.props

        return (
            <div className={styles.cartItem}>
                <div className={styles.contentContainer}>
                    <div className={styles.imgWrapper}>
                        <img className={styles.itemAvatar} src={img}
                             alt={name}/>
                    </div>
                </div>

                <span className={styles.quantity}>{count}</span>

                <div className={styles.btnContainer}>
                    <Button onClick={() => incrementCartItem(id)} className={styles.btn}>+</Button>
                    <Button onClick={() => dicrementCartItem(id)} className={styles.btn}>-</Button>
                    <Button onClick={() => {
                        setModalProps({ id, name });
                        toggleModal(true);
                    }} color="red" className={styles.btn}>DEL</Button>
                </div>

            </div>
        )
    }
}
CartItem.propTypes = {
    carts: PropTypes.array,
    incrementCartItem: PropTypes.func,
    dicrementCartItem: PropTypes.func,
    toggleModal: PropTypes.func,
    setModalProps: PropTypes.func,
    name: PropTypes.string,
    img: PropTypes.string,
    count: PropTypes.number,
    id: PropTypes.number,
}


export default CartItem;

